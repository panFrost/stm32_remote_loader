FROM node:12

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

RUN npm install
# If you are building your code for production
# RUN npm ci --only=production

# Bundle app source
COPY . .

EXPOSE 3000
RUN apt-get update && apt-get install -y \
  libusb-1.0.0-dev
RUN tar -xvf bin/STM32Prog.tar
RUN cp 
CMD [ "node", "index.js" ]
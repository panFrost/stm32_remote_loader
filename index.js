const express = require('express');
const fileUpload = require('express-fileupload');
const cors = require('cors');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const _ = require('lodash');
const { exec } = require('child_process');


const app = express();

// enable files upload
app.use(fileUpload({
    createParentPath: true
}));

//add other middleware
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(morgan('dev'));

//start app 
const port = process.env.PORT || 3000;

var used_platform = process.platform;

app.listen(port, () =>
    console.log(`App is listening on port ${port}.`)
);

app.post('/upload-firmware', async (req, res) => {
    try {
        if (!req.files) {
            res.send({
                status: false,
                message: 'No file uploaded'
            });
        } else {
            //Use the name of the input field (i.e. "avatar") to retrieve the uploaded file
            let firmware_image = req.files.firmware;

            //Use the mv() method to place the file in upload directory (i.e. "uploads")
            firmware_image.mv('./uploads/' + firmware_image.name);

            //send response
            res.send({
                status: true,
                message: 'File is uploaded',
                data: {
                    name: firmware_image.name,
                    mimetype: firmware_image.mimetype,
                    size: firmware_image.size
                }
            });
            let process_location;
            switch (used_platform) {
                case "win32":
                    break;
                case "darwin":
                    process_location="/Applications/STMicroelectronics/STM32Cube/STM32CubeProgrammer/STM32CubeProgrammer.app/Contents/MacOs/bin/STM32_Programmer_CLI --connect port=SWD --erase all --download uploads/stm32_cxx_cmake_backplate.bin 0x8000000 -rst"
                    break;
                case "linux":
                    process_location="./STM32CubeProgrammer/bin/STM32_Programmer.sh --connect port=SWD --erase all --download uploads/stm32_cxx_cmake_backplate.bin 0x8000000 -rst"
                    console.log("Linux platform");
                    break;
            }
            let args = ['-c','port=SWD','-erase','all']
            const ls = exec( process_location ,function (error, stdout, stderr) {
                if (error) {
                  console.log(error.stack);

                  console.log('Error code: '+error.code); 
                  console.log('Signal received: '+error.signal);
                }
                console.log("Process spawn file:"+ls.spawnfile);
                console.log("Process spawn args:"+ls.spawnargs);
                console.log('Child Process STDOUT: '+stdout);
                console.log('Child Process STDERR: '+stderr);
              });
              
              ls.on('exit', function (code) {
                console.log('Child process exited with exit code '+code);
              });
        }
    } catch (err) {
        res.status(500).send(err);
    }
});